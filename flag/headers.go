package flag

const (
	HeaderTerminatedFlag = "X-Terminate-Response"
)

const HeaderTerminatedValue = "terminated"
